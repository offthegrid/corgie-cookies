﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Criterion;

public class DogCannon : MonoBehaviour
{
    public GameObject SpawnVisualsPrefab = null;
    public GameObject SpawnPoint = null;
    public GameObject ModelRoot = null;
    public GameObject DogWalkerPrefab = null;
    public SpriteCollection SpritesCollection;

    public float SpawnTime = 1.0f;

    float timer = 0.0f;
    GameObject visuals = null;

    bool spawned = false;

    // Start is called before the first frame update
    void Start()
    {
        timer = SpawnTime;

        visuals = Instantiate(SpawnVisualsPrefab, SpawnPoint.transform);
    }

    // Update is called once per frame
    void Update()
    {
        GameManager gm = GameManager.Instance;

        if (gm)
        {
            timer -= Time.deltaTime;

            if (timer < 0.0f)
            {
                Destroy(visuals, 0.25f);
                enabled = false;
            }

            if (spawned == false)
            {
                SpawnDogs(gm);
                spawned = true;
            }
        }
    }

    void SpawnDogs(GameManager gm)
    {
        // Make artists
        SpawnDogsOfRole(gm, Role.Art);
        SpawnDogsOfRole(gm, Role.Code);
        SpawnDogsOfRole(gm, Role.Design);
    }

    void SpawnDogsOfRole(GameManager gm, Role role)
    {
        List<GameManager.CreatedDog> old;
        int needed;
        gm.GetDogsForRole(role, out old, out needed);

        foreach (GameManager.CreatedDog oldDog in old)
        {
            GameObject dog = Instantiate(DogWalkerPrefab, ModelRoot.transform);
            dog.transform.position = SpawnPoint.transform.position;

            DogWalker walker = dog.GetComponent<DogWalker>();
            walker.SetProperties(oldDog.role, oldDog.skinIndex);
        }

        for (int i = 0; i < needed; ++i)
        {
            GameObject dog = Instantiate(DogWalkerPrefab, ModelRoot.transform);
            dog.transform.position = SpawnPoint.transform.position;

            DogWalker walker = dog.GetComponent<DogWalker>();
            int skin = Random.Range(0, SpritesCollection.Sprites.Length);
            walker.SetProperties(role, skin);

            gm.RegisterDog(role, skin);
        }
    }
}
