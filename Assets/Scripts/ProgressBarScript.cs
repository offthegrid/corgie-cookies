﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressBarScript : MonoBehaviour
{
    public float BarProgress;

    [SerializeField] private float MaxHeight;
    [SerializeField] private RectTransform trans;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        trans.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, BarProgress * MaxHeight);
    }
}
