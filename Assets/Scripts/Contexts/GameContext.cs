﻿using UnityEngine;

namespace Criterion
{
	public class GameContext : MonoBehaviour
	{
		[Tooltip("Holds a reference to the default game logic that determine what happens after a player action")]
		public PlayerActionManager DefaultPlayerActionManager;

		[Tooltip("Holds a reference to the default score logic that determine how the score is calculated")]
		public ScoreManager DefaultScoreManager;

		private void Awake()
		{
			PlayerActionManager.Instance = DefaultPlayerActionManager;
			ScoreManager.Instance = DefaultScoreManager;
		}
	}
}
