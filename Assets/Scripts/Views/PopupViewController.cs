﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Criterion
{
    [RequireComponent(typeof(PopupView))]
    public class PopupViewController : MonoBehaviour
    {
        public PopupView View;
        public AudioSource SoundOnOpen;
        public GameObject PopupPanel;

        // Start is called before the first frame update
        void Awake()
        {
            GameManager.Instance.OnDevelopmentPhaseEnded -= OnDevelopmentPhaseEnded;
            GameManager.Instance.OnDevelopmentPhaseEnded += OnDevelopmentPhaseEnded;

            View = transform.GetComponent<PopupView>();
            PopupPanel.SetActive(false);
        }
        
        private void OnDevelopmentPhaseEnded(DevelopmentPhase phaseEnded)
        {
            View.UpdatePopupText(phaseEnded.Name);

            PopupPanel.SetActive(true);
            SoundOnOpen.Play();
        }

        public void OnClickContinue()
        {
            PopupPanel.SetActive(false);
            SoundOnOpen.Stop();
            GameManager.Instance.StartDevelopmentPhase();
        }
    }
}