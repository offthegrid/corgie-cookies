﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Criterion.PlayerActionManager;

namespace Criterion
{
	public class SpriteOnCursorView : MonoBehaviour, CookieJarEventsListener, DeskEventsListener
	{
		private Image m_cursor;
		private Text m_info;

		void Awake ()
		{
			m_cursor = transform.GetComponent<Image>();
			m_info = transform.Find("CursorInfo").GetComponent<Text>();
		}

		void Start()
		{
			PlayerActionManager.Instance.AddListener(this);
			PlayerActionManager.Instance.AddCookieJarListener(this);
		}

		// Update is called once per frame
		void Update()
		{
			transform.position = Input.mousePosition;
		}

		void OnEnable()
		{
			PlayerActionManager.Instance.AddListener(this);
			PlayerActionManager.Instance.AddCookieJarListener(this);
		}

		void OnDisable()
		{
			PlayerActionManager.Instance.RemoveListener(this);
			PlayerActionManager.Instance.RemoveCookieJarListener(this);
		}

		void OnDestroy()
		{
			PlayerActionManager.Instance.RemoveListener(this);
			PlayerActionManager.Instance.RemoveCookieJarListener(this);
		}

		public void OnCookieGrabbed(CookieJarInstance jar, CookieInstance cookie)
		{
			if(cookie.Data.Role == Role.Leaf)
			{
				m_info.text = "BUY x" + cookie.TimeMultiplier.ToString() + " COOKIES";
			}
			else
			{
				m_info.text = "PAWS x" + cookie.TimeMultiplier.ToString();
			}
			
			m_cursor.sprite = cookie.Data.Sprite;
			m_info.enabled = true;
			m_cursor.enabled = true;
		}

		public void OnCookiePlacedOnDesk(DeskInstance desk, CookieInstance cookie)
		{
			ClearCursor();
		}

		public void OnCookieBought(CookieJarInstance jar, CookieInstance cookie, int amount)
		{
			ClearCursor();
		}

		private void ClearCursor()
		{
			m_info.text = "";
			m_info.enabled = false;
			m_cursor.sprite = null;
			m_cursor.enabled = false;
		}
	}
}
