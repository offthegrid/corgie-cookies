﻿using UnityEngine;

namespace Criterion
{
	[RequireComponent(typeof(ScoreBarView))]
	public class ScoreBarViewController : MonoBehaviour, ScoreManager.ScoreEventListener
	{
		public ScoreBarView View;

		public virtual void Awake()
		{
			View = transform.GetComponent<ScoreBarView>();
		}

		public virtual void Start()
		{
			ScoreManager.Instance.AddListener(this);
		}

		void OnEnable()
		{
			ScoreManager.Instance.AddListener(this);
		}

		void OnDisable()
		{
			ScoreManager.Instance.RemoveListener(this);
		}

		void OnDestroy()
		{
			ScoreManager.Instance.RemoveListener(this);
		}

		public void OnScoreChanged(int lastContribute, int currentScore)
		{
			View.UpdateValue( Mathf.Clamp((float)currentScore/(float)ScoreManager.Instance.TargetScore, 0.0f, 1.0f));
		}

		public void OnScoreTargetHit(int lastContribute, int currentScore, int targetScore)
		{
		}
	}
}
