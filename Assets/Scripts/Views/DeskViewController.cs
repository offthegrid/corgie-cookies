﻿using UnityEngine;

namespace Criterion
{
	[RequireComponent(typeof(DeskView))]
	public class DeskViewController : MonoBehaviour, DeskInstance.DeskInstanceListener
	{
		public DeskView View;
		protected DeskInstance m_desk;

		public virtual void Awake()
		{
			View = transform.GetComponent<DeskView>();
			m_desk = transform.GetComponent<DeskInstance>();
		}

		public virtual void Start()
		{
			UpdateData(m_desk);
		}

		void OnEnable()
		{
			View.OnDeskSelected += OnDeskSelected;
		}

		void OnDisable()
		{
			View.OnDeskSelected -= OnDeskSelected;
		}

		public virtual void OnDeskSelected()
		{
			m_desk.OnDeskSelected();
		}

		public virtual void UpdateData(DeskInstance desk)
		{
			if (m_desk)
			{
				m_desk.RemoveListener(this);
			}

			m_desk = desk;

			if(desk == null)
			{
				return;
			}

			m_desk.AddListener(this);
			View.SetSprite(m_desk.Data.Sprite);
		}

		public void Activate()
		{
			//View.<something>.interactable = true;
		}

		public void Deactivate()
		{
			//View.<something>.interactable = false;
		}

		public void OnDogAssignedToDesk(DeskInstance desk, DogInstance dog)
		{
			//View.SetDogSprite(dog.GetDogSprite());
		}

		public void OnDogLeftDesk(DeskInstance desk, DogInstance dog)
		{
			View.SetDogSprite(null);
			View.SetCookieSprite(null);
		}

		public void OnCookiePlacedOnDesk(DeskInstance desk, CookieInstance cookie)
		{
			View.SetCookieSprite(cookie.Data.Sprite);
			View.PlayPlaceCookieSound();
		}

		public void OnCookieChewed(float cookieRemaining)
		{
			View.SetCookieFill(cookieRemaining);
		}
	}

}

