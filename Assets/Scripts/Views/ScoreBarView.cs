﻿using UnityEngine;
using UnityEngine.UI;

namespace Criterion
{
	public class ScoreBarView : MonoBehaviour
	{
		public Slider ScoreSlider;
		public float InterpolationTimeInSeconds = 0.0f;

		private float m_currentDelta = 0.0f;
		private float m_oldValue = 0.0f;
		private float m_targetValue = 0.0f;

		public void UpdateValue(float targetValue)
		{
			m_targetValue = targetValue;
			m_oldValue = ScoreSlider.value;
			m_currentDelta = 0.0f;

			if (InterpolationTimeInSeconds <= 0.0f)
			{
				m_oldValue = targetValue;
				ScoreSlider.value = targetValue;
			}
		}

		void Update()
		{
			if (m_oldValue == m_targetValue)
				return;

			m_currentDelta += Time.deltaTime / InterpolationTimeInSeconds;

			float currentValue = Mathf.Lerp(m_oldValue, m_targetValue, m_currentDelta);
			if (m_currentDelta >= 1.0f)
			{
				m_oldValue = m_targetValue;
				m_currentDelta = 0.0f;
			}

			ScoreSlider.value = currentValue;
		}
	}
}
