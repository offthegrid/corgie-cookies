﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Criterion
{
	[RequireComponent(typeof(PlantView))]

	public class PlantViewController : MonoBehaviour
	{
		public PlantView View;
		public Plant PlantData;
		public Cookie Leaf;
		public bool IsLeafReady = false;

		public float CurrentTime = 0.0f;

		public virtual void Awake()
		{
			View = transform.GetComponent<PlantView>();
			IsLeafReady = false;
			CurrentTime = 0.0f;
		}

		void OnEnable()
		{
			View.OnPlantSelected += OnPlantSelected;
		}

		void OnDisable()
		{		
			View.OnPlantSelected -= OnPlantSelected;
		}

		public virtual void OnPlantSelected()
		{
			// reload one leaf and start counting again
			var activeCookie = PlayerActionManager.Instance.GetActiveCookie();

			if(activeCookie != null)
			{
				return;// player already has something in their hands
			}

			// generate new leaf
			var leafToAdd = new CookieInstance
			{
				Data = Leaf,
				TimeMultiplier = Random.Range(1, 4)
			};

			PlayerActionManager.Instance.OnLeafGrabbedFromPlant(leafToAdd);
			View.PlayGrabLeafSound();

			ResetPlant();
		}

		public void ResetPlant()
		{
			CurrentTime = 0.0f;
			IsLeafReady = false;
			Deactivate();
		}

		public void Activate()
		{
			View.GrabLeafButton.interactable = true;
		}

		public void Deactivate()
		{
			View.GrabLeafButton.interactable = false;
		}

		private void Update()
		{
			if(IsLeafReady)
			{
				return;
			}

			CurrentTime += Time.deltaTime;
			View.SetPlantFillAmount( Mathf.Clamp(CurrentTime / PlantData.ReloadTime, 0.0f, 1.0f));

			if (CurrentTime >= PlantData.ReloadTime)
			{
				View.SetPlantFillAmount(1.0f);
				Activate();
				View.PlayLeafReadySound();
				IsLeafReady = true;
			}
		}
	}
}