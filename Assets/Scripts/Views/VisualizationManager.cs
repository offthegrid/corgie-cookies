﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Criterion;

public class VisualizationManager : MonoBehaviour
{
    public GameObject canvasGameArea;
    public GameObject carpetElement;

    public GameObject deskSource = null;
    public GameObject deskGroupSource = null;
    public GameObject dogSource = null;

    class DrawnDesk
    {
        public DeskRegistry source;
        public GameObject drawnObject;
    }
    List<DrawnDesk> drawnDesks = new List<DrawnDesk>();

    class DrawnDeskGroup
    {
        public DeskGroupRegistry source;
        public GameObject drawnObject;
    }
    List<DrawnDeskGroup> drawnDeskGroups = new List<DrawnDeskGroup>();

    class DrawnDog
    {
        public DogWalker source;
        public GameObject drawnObject;
    }
    List<DrawnDog> drawnDogs = new List<DrawnDog>();


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // The floor manager knows about all 3d objects in the sim, we then need to spawn and update 2d representations to reflect them
        FloorManager floorMgr = FloorManager.Instance;
        if (floorMgr == null)
            return;

        UpdateDeskGroupViews(floorMgr);
        UpdateDeskView(floorMgr);
        UpdateDogView(floorMgr);
    }


    void UpdateDeskGroupViews(FloorManager floorMgr)
    {
        List<DeskGroupRegistry> groupsInScene = new List<DeskGroupRegistry>();
        floorMgr.GetDeskGroups(groupsInScene);
        foreach (DeskGroupRegistry group in groupsInScene)
        {
            if (drawnDeskGroups.Exists(a => a.source == group) == false)
            {
                DrawnDeskGroup newGroup = new DrawnDeskGroup();
                newGroup.source = group;
                newGroup.drawnObject = Instantiate(deskGroupSource, canvasGameArea.transform);
                newGroup.drawnObject.name = "Desk Group " + drawnDeskGroups.Count;
                drawnDeskGroups.Add(newGroup);

                // Tie the 2d & 3d scripts together
                DeskRegistry[] registries = group.GetComponentsInChildren<DeskRegistry>();
                DeskInstance[] instances = newGroup.drawnObject.GetComponentsInChildren<DeskInstance>();
                Debug.Assert(registries.Length == instances.Length);
                for (int i = 0; i < registries.Length; ++i)
                {
                    registries[i].Instance = instances[i];
                }

                Vector2 screenPos = MapWorldSpaceToCanvasCoord(floorMgr, group.gameObject.transform.position);
                newGroup.drawnObject.transform.localPosition = screenPos;
            }
        }
    }

    void UpdateDeskView(FloorManager floorMgr)
    {
        List<DeskRegistry> desksInScene = new List<DeskRegistry>();
        floorMgr.GetDesks(desksInScene);
        foreach (DeskRegistry desk in desksInScene)
        {
            if (drawnDesks.Exists(a => a.source == desk) == false)
            {
                DrawnDesk newDesk = new DrawnDesk();
                newDesk.source = desk;
                newDesk.drawnObject = Instantiate(deskSource, canvasGameArea.transform);
                newDesk.drawnObject.name = "Desk " + drawnDesks.Count;
                drawnDesks.Add(newDesk);

                // Tie the 2d & 3d scripts together
                desk.Instance = newDesk.drawnObject.GetComponent<DeskInstance>();

                Vector2 screenPos = MapWorldSpaceToCanvasCoord(floorMgr, desk.gameObject.transform.position);
                newDesk.drawnObject.transform.localPosition = screenPos;
            }
        }
    }
        
    void UpdateDogView(FloorManager floorMgr)
    {
        List<DogWalker> dogsInScene = new List<DogWalker>();
        floorMgr.GetDogs(dogsInScene);
        foreach (DogWalker dog in dogsInScene)
        {
            DrawnDog drawnDog = drawnDogs.Find(a => a.source == dog);
            if (drawnDog == null)
            {
                DrawnDog newDog = new DrawnDog();
                newDog.source = dog;
                newDog.drawnObject = Instantiate(dogSource, canvasGameArea.transform);
                newDog.drawnObject.name = "Dog " + drawnDogs.Count;
                drawnDogs.Add(newDog);

                // Tie the 2d & 3d scripts together
                dog.Instance = newDog.drawnObject.GetComponent<DogInstance>();
                dog.ConfigureInstance();

                drawnDog = newDog;
            }

            Vector2 screenPos = MapWorldSpaceToCanvasCoord(floorMgr, dog.gameObject.transform.position);
            drawnDog.drawnObject.transform.localPosition = screenPos;

            drawnDog.source.Instance.SetFlipped(drawnDog.source.DogDirection == DogWalker.Orientation.Right);
        }
    }

    Vector2 MapWorldSpaceToCanvasCoord(FloorManager floorMgr, Vector3 wsPos)
    {
        // Get dimensions of play space (assuming plane is centred around zero...)
        Vector2 planeSize = floorMgr.GetOfficeExtents();

        // Find input pos as proportion of plane (centred)
//        Vector2 centredNormalizedPos = new Vector2(wsPos.x, wsPos.z) / planeSize;
        Vector2 normalizedPos = new Vector2(wsPos.x, wsPos.z) / planeSize; // (centredNormalizedPos + new Vector2(1.0f, 1.0f)) * 0.5f;

        // Remap to size of canvas
        Rect carpetRect = carpetElement.GetComponent<RectTransform>().rect;
        Vector2 carpetExtents = carpetRect.size * 0.5f;

        Vector2 carpetPos = (carpetExtents * normalizedPos);

        return carpetPos;
    }
}
