﻿using UnityEngine;

namespace Criterion
{
	[RequireComponent(typeof(ScoreView))]
	public class ScoreViewController : MonoBehaviour, ScoreManager.ScoreEventListener
	{
		public ScoreView View;
		
		public virtual void Awake()
		{
			View = transform.GetComponent<ScoreView>();
		}

		public virtual void Start()
		{
			var scoreManager = ScoreManager.Instance;
			View.SetTargetScore(scoreManager.TargetScore.ToString());
			View.SetCurrentScore(scoreManager.GetCurrentScore().ToString());
			ScoreManager.Instance.AddListener(this);
		}

		void OnDisable()
		{
			ScoreManager.Instance.RemoveListener(this);
		}

		void OnDestroy()
		{
			ScoreManager.Instance.RemoveListener(this);
		}

		public void OnScoreChanged(int lastContribute, int currentScore)
		{
			View.SetCurrentScore(currentScore.ToString());
		}

		public void OnScoreTargetHit(int lastContribute, int currentScore, int targetScore)
		{
			View.SetCurrentScore(currentScore.ToString());
			View.SetTargetScore(targetScore.ToString());
		}
	}

}

