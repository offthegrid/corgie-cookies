﻿using UnityEngine;

namespace Criterion
{
    public class MenuViewController : MonoBehaviour
    {
        public void LoadStartMenu()
        {
            GameManager.Instance.LoadGameMode(GameModeType.StartMenu);
        }

        public void LoadGame()
        {
            GameManager.Instance.LoadGameMode(GameModeType.Game);
        }

        public void LoadCredits()
        {
            GameManager.Instance.LoadGameMode(GameModeType.Credits);
        }

        public void QuitGame()
        {
            GameManager.Instance.ExitGame();
        }
    }
}
