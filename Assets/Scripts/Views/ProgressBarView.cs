﻿using UnityEngine;
using UnityEngine.UI;

namespace Criterion
{
	public class ProgressBarView : MonoBehaviour
	{
		public RectTransform DogTransform;
		public Text TimerLabel;
		public Text TimeToFinaling;

        // Phase markers
        public GameObject PhaseMarkerPrefab;
        public Transform PhaseMarkerContainerTransform;

		private GameManager m_gameManager;

		[SerializeField]
		private float StartHeight;

		[SerializeField]
		private float EndHeight;

		private void Start()
		{
			m_gameManager = GameManager.Instance;

            float cumulativePhaseTime = 0.0f;
            foreach (DevelopmentPhase phase in m_gameManager.DevelopmentPhases)
            {
                cumulativePhaseTime += phase.Duration;
                float y = ((cumulativePhaseTime / m_gameManager.TotalDevelopmentTime) * EndHeight) - 242.0f;
                AddPhaseMarker(phase.PhaseSprite, y);
            }
		}

        private void AddPhaseMarker(Sprite sprite, float placementY)
        {
            GameObject phaseMarker = GameObject.Instantiate(PhaseMarkerPrefab);
            Image phaseImage = phaseMarker.transform.GetChild(0).GetComponentInChildren<Image>();
            if (phaseImage != null)
            {
                phaseImage.sprite = sprite;
                phaseImage.SetNativeSize();
            }

            phaseMarker.transform.SetParent(PhaseMarkerContainerTransform);
            phaseMarker.transform.localPosition = new Vector3(0.0f, placementY, 0.0f);
            phaseMarker.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        }

		void Update()
		{
			if(m_gameManager == null)
			{
				return;
			}

			float finalingTime = m_gameManager.TotalDevelopmentTime - m_gameManager.TotalElapsedTime;
			float finalingMinutes = Mathf.Floor(finalingTime / 60);
			float finalingSeconds = finalingTime % 60;

			TimeToFinaling.text = string.Format("{0:00}:{1:00}", finalingMinutes, finalingSeconds);

			float phaseTimer = m_gameManager.PhaseTime - m_gameManager.CurrentPhaseTime;
			float phaseMinutes = Mathf.Floor(phaseTimer / 60);
			float phaseSeconds = phaseTimer % 60;

			//TimerLabel.text = string.Format("{0:00}:{1:00}", phaseMinutes, phaseSeconds);
			DogTransform.localPosition = new Vector3(0.0f, ((m_gameManager.TotalElapsedTime / m_gameManager.TotalDevelopmentTime) * EndHeight) - 242.0f, 0.0f);
		}
	}
}