﻿using System;
using UnityEngine;

namespace Criterion
{
	[RequireComponent(typeof(CookieJarView))]
	public class CookieJarViewController : MonoBehaviour, PlayerActionManager.CookieJarEventsListener, PlayerActionManager.DeskEventsListener
	{
		public CookieJarView View;
		public int StartCookieAmount = 1;
		protected CookieJarInstance m_cookieJar;

		public virtual void Awake()
		{
			View = transform.GetComponent<CookieJarView>();
			m_cookieJar = transform.GetComponent<CookieJarInstance>(); 
		}

		public virtual void Start()
		{
			m_cookieJar.AddCookies(StartCookieAmount);
			UpdateData(m_cookieJar);

			PlayerActionManager.Instance.AddListener(this);
			PlayerActionManager.Instance.AddCookieJarListener(this);
		}

		void OnEnable()
		{
			View.OnJarSelected += OnJarSelected;
			PlayerActionManager.Instance.AddListener(this);
			PlayerActionManager.Instance.AddCookieJarListener(this);
		}

		void OnDisable()
		{
			PlayerActionManager.Instance.RemoveListener(this);
			PlayerActionManager.Instance.RemoveCookieJarListener(this);
			View.OnJarSelected -= OnJarSelected;
		}

		void OnDestroy()
		{
			PlayerActionManager.Instance.RemoveListener(this);
			PlayerActionManager.Instance.RemoveCookieJarListener(this);
		}

		public virtual void OnJarSelected()
		{
			

			//check if the player is holding a leaf or another cookie
			var activeCookie = PlayerActionManager.Instance.GetActiveCookie();

			if (m_cookieJar.IsEmpty() && activeCookie == null)
			{
				return;
			}

			// grab a cookie
			if (activeCookie == null || activeCookie.Data.Role != Role.Leaf)
			{
				if(!View.GrabCookieButton.IsInteractable())
				{
					return;
				}

				var cookie = m_cookieJar.GrabCookie();
				UpdateData(m_cookieJar);
				View.PlayGrabCookieSound();
				PlayerActionManager.Instance.OnCookieGrabbed(m_cookieJar, cookie);
			}
			// buy some cookies (if the jar is not full?)
			else
			{
				var cookieAmount = activeCookie.TimeMultiplier;
				m_cookieJar.AddCookies(cookieAmount);
				UpdateData(m_cookieJar);
				View.PlayBuyCookieSound();
				PlayerActionManager.Instance.OnCookieBought(m_cookieJar, activeCookie, cookieAmount);
			}
		}

		public virtual void UpdateData(CookieJarInstance cookieJar)
		{
			m_cookieJar = cookieJar;
			View.SetSprite(m_cookieJar.Data.Sprite);
			View.SetContentSprite(m_cookieJar.Data.CookieType.ManySprite);
			View.SetCounter(m_cookieJar.Data.CookieType.Role.ToString().ToUpper() +  " x" + m_cookieJar.CookiesAmount.ToString());
			View.SetJarFillAmount((float)m_cookieJar.CookiesAmount / m_cookieJar.Data.Capacity);

			if (m_cookieJar.IsEmpty())
			{
				View.SetJarFillAmount(0.0f);
				Deactivate();
			}
		}

		public void Activate()
		{
			View.GrabCookieButton.interactable = true;
		}

		public void Deactivate()
		{
			View.GrabCookieButton.interactable = false;
		}

		public void OnCookieGrabbed(CookieJarInstance jar, CookieInstance cookie)
		{
			if(jar == null)
			{
				// it's coming from a plant
				return;
			}

			if(m_cookieJar.Data.CookieType.Role == cookie.Data.Role)
			{
				Deactivate();
			}
			else if (!m_cookieJar.IsEmpty())
			{
				Activate();
			}
		}

		public void OnCookiePlacedOnDesk(DeskInstance desk, CookieInstance cookie)
		{
			if (!m_cookieJar.IsEmpty())
			{
				Activate();
			}
		}

		public void OnCookieBought(CookieJarInstance jar, CookieInstance cookie, int amount)
		{
			if(m_cookieJar.IsEmpty())
			{
				return;
			}
			
			if(jar == m_cookieJar || m_cookieJar.Data.CookieType.Role == Role.Leaf)
			{
				Activate();
			}
			
		}
	}
}
