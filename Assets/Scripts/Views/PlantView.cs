﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Criterion
{
	public class PlantView : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
	{
		public Button GrabLeafButton;
		public AudioSource LeafAudioSource;
		public AudioClip LeafReadySound;
		public AudioClip GrabLeafSound;

		public Image PlantFillImage;
		public Image PlantGlowImage;
		public Image LeafIconImage;

		public delegate void SelectPlant();
		public SelectPlant OnPlantSelected;


		private void Awake()
		{
			if (GrabLeafButton == null)
			{
				GrabLeafButton = transform.GetComponent<Button>();
			}

			if (PlantFillImage == null)
			{
				PlantFillImage = transform.Find("PlantFill").GetComponent<Image>();
			}

			if (PlantGlowImage == null)
			{
				PlantGlowImage = transform.Find("PlantGlow").GetComponent<Image>();
			}

			if (LeafIconImage == null)
			{
				LeafIconImage = transform.Find("LeafIcon").GetComponent<Image>();
			}

			if (LeafAudioSource == null)
			{
				LeafAudioSource = transform.GetComponent<AudioSource>();
			}
		}

		void Start()
		{
			SetPlantFillAmount(0.0f);
		}

		public void SetPlantFillAmount(float amount)
		{
			PlantFillImage.fillAmount = amount;
			PlantGlowImage.fillAmount = amount;
			LeafIconImage.enabled = amount == 1.0f;
		}

		public void PlayLeafReadySound()
		{
			LeafAudioSource.PlayOneShot(LeafReadySound);
		}

		public void PlayGrabLeafSound()
		{
			LeafAudioSource.PlayOneShot(GrabLeafSound);
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			if (!GrabLeafButton.IsInteractable())
			{
				return;
			}
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			if(!GrabLeafButton.IsInteractable())
			{
				return;
			}

			OnPlantSelected();
		}
	}
}