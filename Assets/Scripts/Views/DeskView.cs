﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Criterion
{
	public class DeskView : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
	{
		public Button PlaceCookieButton;

		public Image DeskImage;
		public Image CookieImage;
		public Image DogImage;

		public Sprite DefaultSprite;

		public AudioSource PlaceCookieSound;

		public delegate void SelectDesk();
		public SelectDesk OnDeskSelected;

		private void Awake()
		{
			if (PlaceCookieButton == null)
			{
				PlaceCookieButton = transform.GetComponent<Button>();
			}

			if (DeskImage == null)
			{
				DeskImage = transform.GetComponent<Image>();
			}

			if (CookieImage == null)
			{
				CookieImage = transform.Find("CookieSlotTarget").GetComponent<Image>();
			}

			if (DogImage == null)
			{
				DogImage = transform.Find("DogSlotTarget").GetComponent<Image>();
			}

			if (PlaceCookieSound == null)
			{
				PlaceCookieSound = transform.GetComponent<AudioSource>();
			}
		}

		public void SetSprite(Sprite deskSprite)
		{
			if (deskSprite != null)
			{
				DeskImage.sprite = deskSprite;
				return;
			}

			DeskImage.sprite = DefaultSprite;
		}

		public void SetCookieSprite(Sprite cookieSprite)
		{
			if (cookieSprite != null)
			{
				CookieImage.sprite = cookieSprite;
				CookieImage.fillAmount = 1.0f;
				CookieImage.enabled = true;
				return;
			}

			CookieImage.enabled = false;
		}

		public void SetCookieFill(float fill)
		{
			CookieImage.fillAmount = fill;
		}

		public void SetDogSprite(Sprite dogSprite)
		{
			if (dogSprite != null)
			{
				DogImage.sprite = dogSprite;
				DogImage.enabled = true;
				return;
			}

			DogImage.enabled = false;
		}

		public void PlayPlaceCookieSound()
		{
			PlaceCookieSound.Play();
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			if (!PlaceCookieButton.IsInteractable())
			{
				return;
			}
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			if (!PlaceCookieButton.IsInteractable())
			{
				return;
			}
			
			OnDeskSelected();
		}
	}
}
