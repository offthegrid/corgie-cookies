﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Criterion
{
	public class ScoreView : MonoBehaviour
	{
		public Text CurrentScore;
		public Text TargetScore;

		private void Awake()
		{
			if (CurrentScore == null)
			{
				CurrentScore = transform.Find("CurrentScore").GetComponent<Text>();
			}

			if (TargetScore == null)
			{
				TargetScore = transform.Find("TargetScore").GetComponent<Text>();
			}
		}

		public void SetCurrentScore(string score)
		{
			CurrentScore.text = score;
		}

		public void SetTargetScore(string score)
		{
			TargetScore.text = score;
		}
	}
}
