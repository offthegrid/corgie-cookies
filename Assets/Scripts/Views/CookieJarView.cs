﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Criterion
{
	public class CookieJarView : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
	{
		public Button GrabCookieButton;

		public Image CookieJarImage;
		public Image CookieJarLabelImage;
		public Image CookieJarContentImage;
		public Text CookieJarCounter;

		public Sprite DefaultSprite;
		public Sprite DefaultContentSprite;

		public AudioSource GrabCookieSound;

		public AudioClip BuyCookieClip;

		public delegate void SelectCookieJar();
		public SelectCookieJar OnJarSelected;	

		public bool IsSelected { get; private set; }

		public bool IsInteractable { get; private set; }

		//private float m_lastPointerDown = 0.0f;

		private void Awake()
		{
			if (GrabCookieButton == null)
			{
				GrabCookieButton = transform.GetComponent<Button>();
			}

			if (CookieJarImage == null)
			{
				CookieJarImage = transform.Find("EmptyJar").GetComponent<Image>();
			}

			if (CookieJarLabelImage == null)
			{
				CookieJarLabelImage = transform.Find("JarLabel").GetComponent<Image>();
			}

			if (CookieJarContentImage == null)
			{
				CookieJarContentImage = transform.Find("JarContent").GetComponent<Image>();
			}

			if (CookieJarCounter == null)
			{
				CookieJarCounter = transform.Find("CounterBubble/Counter").GetComponent<Text>();
			}

			if (GrabCookieSound == null)
			{
				GrabCookieSound = transform.GetComponent<AudioSource>();
			}
		}

		public void SetSprite(Sprite cookieJarSprite)
		{
			if (cookieJarSprite != null)
			{
				CookieJarImage.sprite = cookieJarSprite;
				return;
			}

			CookieJarImage.sprite = DefaultSprite;
		}

		public void SetContentSprite(Sprite cookieJarContentSprite)
		{
			if (CookieJarContentImage != null)
			{
				CookieJarContentImage.sprite = cookieJarContentSprite;
				return;
			}

			CookieJarContentImage.sprite = DefaultContentSprite;
		}

		public void SetLabelSprite(Sprite cookieJarLabelSprite)
		{
			if (CookieJarLabelImage != null)
			{
				CookieJarLabelImage.sprite = cookieJarLabelSprite;
				return;
			}
		}

		public void SetJarFillAmount(float amount)
		{
			CookieJarContentImage.fillAmount = amount;
		}

		public void SetCounter(string counter)
		{
			CookieJarCounter.text = counter;
		}

		public void PlayGrabCookieSound()
		{
			GrabCookieSound.Play();
		}

		public void PlayBuyCookieSound()
		{
			GrabCookieSound.PlayOneShot(BuyCookieClip);
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			if (!GrabCookieButton.IsInteractable())
			{
				return;
			}
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			// grab cookie event
			OnJarSelected();
		}
	}
}
