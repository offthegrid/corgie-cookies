﻿using UnityEngine;
using UnityEngine.UI;

namespace Criterion
{
    public class PopupView : MonoBehaviour
    {
        public string PopupFormatString;
        public Text PopupMessage;

        void Awake()
        {
            PopupMessage.text = string.Empty;
        }
       
        public void UpdatePopupText(string text)
        {
            PopupMessage.text = string.Format(PopupFormatString, text);
        }
    }
}