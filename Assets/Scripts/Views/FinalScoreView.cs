﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Criterion;

public class FinalScoreView : MonoBehaviour
{
    public Text score;
    public Image laptopBG;

    [SerializeField] private float duration = 1f;

    private Color lowColor = new Color(0.827451f, 0.3843138f, 0.4041542f, 1f);
    private Color midColor = new Color (0.7876408f, 0.827451f, 0.3843138f, 1f);
    private Color highColor = new Color (0.3843138f, 0.8274511f, 0.509804f, 1f);

    private float lerp = 0f;
    private int finalScore = (int)(((float)ScoreManager.Instance.CurrentScore / (float)ScoreManager.Instance.TargetScore) * 100.0f);

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        lerp += Time.deltaTime / duration;
        score.text = ((int)Mathf.Lerp(0, finalScore, lerp)).ToString();

        if (finalScore <= 40)
        {
            laptopBG.color = lowColor;
        }
        else if (finalScore >= 70)
        {
            laptopBG.color = Color.Lerp(lowColor, highColor, lerp);
        }
        else
        {
            laptopBG.color = Color.Lerp(lowColor, midColor, lerp);
        }
    }
}
