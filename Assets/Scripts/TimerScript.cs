﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{
    public Text txt;
    public RectTransform trans;
    public float TimerLength;

    [SerializeField] private float StartHeight;
    [SerializeField] private float EndHeight;

    private float timer = 0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        txt.text = timer.ToString("F");

        if (timer <= TimerLength)
        {
            //transform.Translate(0, ((timer / TimerLength) * EndHeight), 0);
            trans.localPosition = new Vector3(0, (((timer / TimerLength) * EndHeight)-242), 0);
        }
    }
}
