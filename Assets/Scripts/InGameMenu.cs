﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameMenu : MonoBehaviour
{
    public KeyCode ToggleMenuKey;
    public GameObject MenuPanel;
    public GameObject MenuButton;
    public bool PauseGameplay;

    private bool m_isVisible;

    void Awake()
    {
        MenuPanel.SetActive(false);
        MenuButton.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(ToggleMenuKey))
        {
            ToggleMenu();
        }
    }

    public void ToggleMenu()
    {
        m_isVisible = !m_isVisible;
        MenuPanel.SetActive(m_isVisible);
        MenuButton.SetActive(!m_isVisible);

        if (PauseGameplay)
            Time.timeScale = m_isVisible ? 0.0f : 1.0f;
    }
}
