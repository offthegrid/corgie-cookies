﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Criterion
{
    public class GameManager : MonoBehaviour
    {
        public GameModeType StartGameMode;

        public GameMode[] GameModes;
        private GameModeType m_currentGameMode;

        // Development
        public DevelopmentPhase[] DevelopmentPhases;
        public float TotalDevelopmentTime { get; private set; }
        public float TotalElapsedTime { get; private set; }
        public float PhaseTime { get; private set; }
        public float CurrentPhaseTime { get; private set; }

        private int m_currentPhase;

        public DevelopmentPhase LastDevelopmentPhase;

        private enum DevelopmentState
        {
            NotStarted,
            Setup,
            InProgress,
            Complete
        }
        private DevelopmentState m_developmentState;

        private static GameManager s_instance;
        public static GameManager Instance
        {
            get
            {
                if (!s_instance)
                {
                    s_instance = GameObject.FindObjectOfType<GameManager>();
                }

                return s_instance;
            }
            private set
            {
                s_instance = value;
            }
        }

        public delegate void OnDevelopmentPhaseEndedDelegate(DevelopmentPhase phaseEnded);
        public OnDevelopmentPhaseEndedDelegate OnDevelopmentPhaseEnded;

        private void RestartGame()
        {
            // Clear old cache of dogs
            TargetArtDogs = 0;
            TargetSEDogs = 0;
            TargetDesignDogs = 0;
            existingDogs.Clear();

            ScoreManager.Instance.CurrentScore = 0;

            m_currentPhase = -1;
            m_developmentState = DevelopmentState.NotStarted;

            TotalDevelopmentTime = 0.0f;
            TotalElapsedTime = 0.0f;
            PhaseTime = 0.0f;
            CurrentPhaseTime = 0.0f;

            foreach (var phase in DevelopmentPhases)
            {
                TotalDevelopmentTime += phase.Duration;
            }

            SetupNextDevelopmentPhase();
            StartDevelopmentPhase();
        }

        private void UnloadCurrentGameMode()
        {
            foreach (GameMode gameMode in GameModes)
            {
                if (gameMode.Type == m_currentGameMode)
                {
                    foreach (string sceneName in gameMode.SceneList)
                    {
                        SceneManager.UnloadSceneAsync(sceneName);
                    }
                    break;
                }
            }

            // special case handling
            switch (m_currentGameMode)
            {
                case GameModeType.Game:
                    if (m_currentPhase >= 0)
                    {
                        SceneManager.UnloadSceneAsync(DevelopmentPhases[m_currentPhase].SceneName, UnloadSceneOptions.None);
                        m_currentPhase = -1;
                    }
                    break;
                default:
                    break;
            }
        }

        public void LoadGameMode(GameModeType gameScene)
        {
            UnloadCurrentGameMode();
            m_currentGameMode = gameScene;
            Time.timeScale = 1.0f;

            foreach (GameMode gameMode in GameModes)
            {
                if (gameMode.Type == m_currentGameMode)
                {
                    foreach (string sceneName in gameMode.SceneList)
                    {
                        SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
                    }
                    break;
                }
            }

            switch (gameScene)
            {
                case GameModeType.Game:
                    RestartGame();
                    break;
                default:
                    break;
            }
        }

        public void SetupNextDevelopmentPhase()
        {
            if (m_currentGameMode != GameModeType.Game)
                return;

            if (m_currentPhase >= 0)
            {
                var currentPhase = DevelopmentPhases[m_currentPhase];

                SceneManager.UnloadSceneAsync(currentPhase.SceneName, UnloadSceneOptions.None);
                if (LastDevelopmentPhase.Name == currentPhase.Name)
                {
                    LoadGameMode(GameModeType.Score);
                    return;
                }
                else if (OnDevelopmentPhaseEnded != null)
                {
                    OnDevelopmentPhaseEnded(currentPhase);
                }
            }

            if (DevelopmentPhases.Length > 0 && m_currentPhase < DevelopmentPhases.Length - 1)
            {
                m_developmentState = DevelopmentState.Setup;

                var phase = DevelopmentPhases[++m_currentPhase];
                SceneManager.LoadScene(phase.SceneName, LoadSceneMode.Additive);

                PhaseTime = phase.Duration;
                CurrentPhaseTime = 0.0f;

                CreateDogs(phase.NewArtists, phase.NewEngineers, phase.NewDesigners);
            }
            else
            {
                m_developmentState = DevelopmentState.Complete;
                TotalElapsedTime = TotalDevelopmentTime;
                m_currentPhase = -1;
            }
        }

        public void StartDevelopmentPhase()
        {
            if (m_developmentState == DevelopmentState.Setup)
                m_developmentState = DevelopmentState.InProgress;
        }

        public void ExitGame()
        {
            Application.Quit();
        }

        void Awake()
        {
            m_currentGameMode = GameModeType.Setup;
        }

        // Start is called before the first frame update
        void Start()
        {
            if (StartGameMode != GameModeType.Setup)
            {
                LoadGameMode(StartGameMode);
            }
        }

        void Update()
        {
            if (m_currentGameMode != GameModeType.Game || m_developmentState != DevelopmentState.InProgress)
                return;

            TotalElapsedTime += Time.deltaTime;
            CurrentPhaseTime += Time.deltaTime;

            if (CurrentPhaseTime >= PhaseTime)
            {
                SetupNextDevelopmentPhase();
            }
        }

        public int TargetArtDogs = 0;
        public int TargetSEDogs = 0;
        public int TargetDesignDogs = 0;

        public struct CreatedDog
        {
            public Role role;
            public int skinIndex;
        }
        List<CreatedDog> existingDogs = new List<CreatedDog>();

        void CreateDogs(int art, int se, int design)
        {
            TargetArtDogs += art;
            TargetSEDogs += se;
            TargetDesignDogs += design;
        }        

        public void GetDogsForRole(Role role, out List<CreatedDog> oldDogs, out int newDogs)
        {
            oldDogs = existingDogs.Where(a => a.role == role).ToList();

            int target = (role == Role.Art) ? TargetArtDogs : (role == Role.Code) ? TargetSEDogs : TargetDesignDogs;
            newDogs = target - oldDogs.Count;
        }

        public void RegisterDog(Role role, int skin)
        {
            CreatedDog newDog = new CreatedDog();
            newDog.role = role;
            newDog.skinIndex = skin;
            existingDogs.Add(newDog);
        }
    }
}