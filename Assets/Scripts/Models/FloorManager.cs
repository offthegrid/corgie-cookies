﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Criterion;

public class FloorManager : MonoBehaviour
{
    public interface Listener
    {
        void OnDogsChanged();
        void OnDesksChanged();
        void OnDeskGroupsChanged();
    }

    private List<Listener> m_listeners = new List<Listener>();
    public void AddListener(Listener listener)
    {
        if (m_listeners.Contains(listener))
        {
            return;
        }

        m_listeners.Add(listener);
    }

    public void RemoveListener(Listener listener)
    {
        if (!m_listeners.Contains(listener))
        {
            return;
        }

        m_listeners.Remove(listener);
    }

    public GameObject floorPlane;

    static FloorManager instance = null;
    public static FloorManager Instance { get { return instance; } }

    MeshRenderer floorPlaneRenderer = null;

    List<DeskRegistry> desks = new List<DeskRegistry>();
    List<DeskGroupRegistry> deskGroups = new List<DeskGroupRegistry>();
    List<DogWalker> dogs = new List<DogWalker>();

    // Start is called before the first frame update
    void Start()
    {
        Debug.Assert(instance == null, "Multiple instances of FloorManager in scene!");
        instance = this;

        floorPlaneRenderer = floorPlane.GetComponent<MeshRenderer>();

        MeshRenderer[] allRenderers = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer meshRenderer in allRenderers)
        {
//            meshRenderer.enabled = false;
        }
    }

    private void OnDestroy()
    {
        desks.Clear();
        deskGroups.Clear();
        dogs.Clear();

        foreach (var listener in m_listeners)
        {
            listener.OnDesksChanged();
            listener.OnDeskGroupsChanged();
            listener.OnDogsChanged();
        }

        instance = null;
    }

    private void OnStart()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Vector2 GetOfficeExtents()
    {
        Vector3 extents = floorPlaneRenderer.bounds.extents;
        return new Vector2(extents.x, extents.z);
    }

    public void GetDesks(IList<DeskRegistry> desksOut)
    {
        foreach (DeskRegistry desk in desks)
        {
            desksOut.Add(desk);
        }
    }

    public struct SeatInfo
    {
        public DeskRegistry sourceDesk;
        public Vector3 position;
    }

    public void GetDeskSeats(IList<SeatInfo> deskSeatPositions)
    {
        foreach (DeskGroupRegistry deskGroup in deskGroups)
        {
            deskGroup.GetSeatPositions(deskSeatPositions);
        }

        foreach (DeskRegistry desk in desks)
        {
            desk.GetSeatPositions(deskSeatPositions);
        }
    }

    public void AddDesk(DeskRegistry newDesk)
    {
        desks.Add(newDesk);

        foreach (var listener in m_listeners)
        {
            listener.OnDesksChanged();
        }
    }

    public void RemoveDesk(DeskRegistry oldDesk)
    {
        desks.Remove(oldDesk);

        foreach (var listener in m_listeners)
        {
            listener.OnDesksChanged();
        }
    }

    public void GetDeskGroups(IList<DeskGroupRegistry> deskGroupsOut)
    {
        foreach (DeskGroupRegistry deskGroup in deskGroups)
        {
            deskGroupsOut.Add(deskGroup);
        }
    }

    public void AddDeskGroup(DeskGroupRegistry newDesk)
    {
        deskGroups.Add(newDesk);

        foreach (var listener in m_listeners)
        {
            listener.OnDeskGroupsChanged();
        }
    }

    public void RemoveDeskGroup(DeskGroupRegistry oldDesk)
    {
        deskGroups.Remove(oldDesk);

        foreach (var listener in m_listeners)
        {
            listener.OnDeskGroupsChanged();
        }
    }

    public void GetDogs(IList<DogWalker> dogsOut)
    {
        foreach (DogWalker dog in dogs)
        {
            dogsOut.Add(dog);
        }
    }

    public void AddDog(DogWalker newDog)
    {
        dogs.Add(newDog);

        foreach (var listener in m_listeners)
        {
            listener.OnDogsChanged();
        }
    }

    public void RemoveDog(DogWalker oldDog)
    { 
        dogs.Remove(oldDog);

        foreach (var listener in m_listeners)
        {
            listener.OnDogsChanged();
        }
    }
}
