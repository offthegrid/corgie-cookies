﻿using UnityEngine;

namespace Criterion
{
	[CreateAssetMenu(fileName = "Plant", menuName = "Data/Plant")]
	public class Plant : ScriptableObject
	{
		[Tooltip("How seconds the plants need to generate a leaf")]
		public int ReloadTime;

		[Tooltip("The data type of cookies/leaf the plant will reward")]
		public Cookie CookieType;
	}
}
