﻿using UnityEngine;

namespace Criterion
{
	[CreateAssetMenu(fileName = "CookieJar", menuName = "Data/CookieJar")]
	public class CookieJar : ScriptableObject
	{
		[Tooltip("How many cookies the jar can contains")]
		public int Capacity;

		[Tooltip("The data type of cookies the jar contains")]
		public Cookie CookieType;

		[Tooltip("Default Sprite that represent the jar")]
		public Sprite Sprite;

		[Tooltip("Default Sprite that represent the label on the jar")]
		public Sprite LabelSprite;
	}
}
