﻿using System.Collections.Generic;
using UnityEngine;

namespace Criterion
{
    [CreateAssetMenu(fileName = "DogSprites", menuName = "Data/DogSprites")]
    public class DogSprites : ScriptableObject
    {
        [Tooltip("Sprite representing the standing position of the dog")]
        public Sprite Standing;

        [Tooltip("Sprite representing the dog sitting")]
        public Sprite Sitting;
    }
}