﻿using UnityEngine;
using System.Collections.Generic;

namespace Criterion
{
	public class DeskGroup : MonoBehaviour
	{
		public List<DeskInstance> Desks;

		public virtual void Update()
		{
			var deltaTime = Time.deltaTime;

			foreach (var desk in Desks)
			{
				if(!desk.HasDog())
				{
					continue;
				}

				if(desk.HasCookie())
				{
					desk.ChewOnCookie(deltaTime);
				}
			}
		}

	}
}
