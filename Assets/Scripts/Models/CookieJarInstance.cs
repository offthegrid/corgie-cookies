﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

namespace Criterion
{
	public class CookieJarInstance : MonoBehaviour
	{
		public int CookiesAmount
		{
			get { return Cookies.Count; }
			private set { }
		}

		[Tooltip("The cookies inside the jar")]
		public Queue<CookieInstance> Cookies = new Queue<CookieInstance>();

		[Tooltip("The data model of the cookie jar")]
		public CookieJar Data;

		/// <summary>
		/// Return true if there as many cookies as the jar max capacity
		/// </summary>
		/// <returns>bool</returns>
		public bool IsFull()
		{
			return Cookies.Count >= Data.Capacity;
		}

		/// <summary>
		/// Return true if there are no cookies left in the jar
		/// </summary>
		/// <returns>bool</returns>
		public bool IsEmpty()
		{
			return Cookies.Count == 0;
		}

		/// <summary>
		/// Return the next cookie in the jar
		/// </summary>
		/// <returns>CookieInstance</returns>
		public CookieInstance GrabCookie()
		{
			if(!IsEmpty())
			{
				return Cookies.Dequeue();
			}

			return null;
		}

		/// <summary>
		/// Add <paramref name="numOfCookies"/> to the jar or until the jar is full
		/// </summary>
		/// <returns>The number of cookies in the jar after the adding operation</returns>
		public int AddCookies(int numOfCookies)
		{
			Assert.IsTrue(numOfCookies >= 0, "Cannot add a negative amount of cookies to the jar");

			int i = numOfCookies;
			while(i > 0 && !IsFull())
			{
				var cookieToAdd = new CookieInstance
				{
					Data = Data.CookieType,
					TimeMultiplier = Random.Range(1, 4)
				};
				Cookies.Enqueue(cookieToAdd);
				--i;
			}
			
			return CookiesAmount;
		}
	}
}
