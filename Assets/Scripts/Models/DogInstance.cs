﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using Criterion;

namespace Criterion
{
    //[RequireComponent(typeof(Button))]
    [RequireComponent(typeof(Image))]
    public class DogInstance : MonoBehaviour, IPointerDownHandler
    {
        //---------------------------------------------------------------------
        // Public Members
        //---------------------------------------------------------------------
        public Dog Data;
        public SpriteCollection SpritesCollection;

        public GameObject DirectionChangedFx = null;
        public float DirectionChangedFxTime = 3.0f;

        public const float DistractionThreshold = 0.2f;
        public const float AgitationThreshold = 0.5f;

        //---------------------------------------------------------------------
        // Private Members
        //---------------------------------------------------------------------
        private DogSprites  SpriteLibrary;
        private Image       DogImage;
        private Image       RoleImage;
        private float       Satisfaction = 1.0f;
        private bool        Distraction = false;
        private bool        Flipped = false;

        private DogInstance[] Neighbors;

        //---------------------------------------------------------------------
        // Public Functions
        //---------------------------------------------------------------------
        public bool IsAgitated()
        {
            return Satisfaction > AgitationThreshold;
        }

        public bool IsDistracted()
        {
            return Distraction;
        }

        public float GetProductivity()
        {
            return Satisfaction;
        }

        public void SetSitting(bool isSitting)
        {
            if (isSitting)
            {
                DogImage.sprite = SpriteLibrary.Sitting;
            }
            else
            {
                DogImage.sprite = SpriteLibrary.Standing;
            }

            RoleImage.enabled = isSitting;
        }

        public void SetFlipped(bool flipped)
        {
            if (flipped)
            {
                DogImage.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f); 
            }
            else
            {
                DogImage.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            }

            Flipped = flipped;
        }

        public void PathChanged()
        {
            if (DirectionChangedFx)
            {
                GameObject newDirectionFx = Instantiate(DirectionChangedFx, this.gameObject.transform);
                Destroy(newDirectionFx, DirectionChangedFxTime);
            }
        }

        public void Distract()
        {
            //Distraction = true;
            //SetSitting(false);
        }

        public void BeginMilestone(DogInstance[] dogsAtDesk)
        {
            // When the desk logic is in place we do something like this
            int newNeighbors = dogsAtDesk.Length;

            // Not too bad, but still some garbage big-O if you concider all the dogs
            foreach (DogInstance dog in dogsAtDesk)
            {
                foreach (DogInstance neighbor in Neighbors)
                {
                    if (neighbor == dog)
                    {
                        --newNeighbors;
                    }
                }
            }

            // Crunch the numbers
            Satisfaction -= 0.1f * (dogsAtDesk.Length - newNeighbors);
            Satisfaction += 0.1f * (newNeighbors - 1); // We don't count ourselves as a fresh neighbor
            Satisfaction = Mathf.Clamp(Satisfaction, 0.0f, 1.0f);

            // Store the current neighbors
            Neighbors = dogsAtDesk;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            //if (Distraction)
            //{
            //    Distraction = false;
            //    if (Satisfaction <= DistractionThreshold)
            //    {
            //        Satisfaction += DistractionThreshold;
            //        Satisfaction = Mathf.Clamp(Satisfaction, 0.0f, 1.0f);
            //    }
            //}
        }

        public void SetDogRole(in Role newRole)
        {
            Data.Role = newRole;
			
			foreach (JobSprites job in SpritesCollection.JobBubbles)
			{
				if (job.Role == newRole)
                {
                    RoleImage.sprite = job.RoleImage;
					return;
				}
            }
        }

        public void SetDogSkin(int newSkinIndex)
        {
            Debug.Assert(newSkinIndex < SpritesCollection.Sprites.Length);
            SpriteLibrary = SpritesCollection.Sprites[newSkinIndex];

            SetSitting(RoleImage.enabled); // Force update
        }

        //public void Update()
        //{
        //    if (Satisfaction <= DistractionThreshold)
        //    {
        //        Distraction = true;
        //        SetSitting(false);
        //    }
        //}

        public Sprite GetDogSprite()
		{
			return transform.GetComponent<Image>().sprite;
		}

		//---------------------------------------------------------------------
		// Private Functions
		//---------------------------------------------------------------------
		private void Awake()
		{
			// Turn off the button
			GetComponent<Button>().enabled = false;

			int selectedSprite = Random.Range(0, SpritesCollection.Sprites.Length);
            SpriteLibrary = SpritesCollection.Sprites[selectedSprite];
            //SpritesCollection = null; // We no longer need the collection

            DogImage = GetComponent<Image>();

			// Dirty but for now the bubble is always the second image
			RoleImage = GetComponentsInChildren<Image>()[1];

            // Initialize our dog to be standing
            SetSitting(false);
        }
    }
}
