﻿using System.Collections.Generic;
using UnityEngine;

namespace Criterion
{
    [CreateAssetMenu(fileName = "Dog", menuName = "Data/Dog")]
    public class Dog : ScriptableObject
    {
        [Tooltip("What role this dog is associated with")]
        public Role Role;
    }
}