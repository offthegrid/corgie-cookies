﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeskGroupRegistry : MonoBehaviour
{
    public List<GameObject> deskLocators = new List<GameObject>();



    // Start is called before the first frame update
    void Start()
    {
    }

    private void OnDestroy()
    {
        FloorManager floorMgr = FloorManager.Instance;
        if (floorMgr)
            floorMgr.RemoveDeskGroup(this);
    }

    // Update is called once per frame
    void Update()
    {
        FloorManager floorMgr = FloorManager.Instance;
        if (floorMgr)
        {
            floorMgr.AddDeskGroup(this);
            this.enabled = false;
        }
    }

    public void GetSeatPositions(IList<FloorManager.SeatInfo> deskSeatPositions)
    {
        foreach (GameObject desk in deskLocators)
        {
            DeskRegistry registry = desk.GetComponent<DeskRegistry>();
            registry.GetSeatPositions(deskSeatPositions);
        }
    }

    public void GetDeskRegistries(IList<DeskRegistry> desks)
    {
        foreach (GameObject desk in deskLocators)
        {
            DeskRegistry registry = desk.GetComponent<DeskRegistry>();
            desks.Add(registry);
        }
    }
}
