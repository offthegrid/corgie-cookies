﻿using UnityEngine;

namespace Criterion
{
	public class CookieInstance
	{
		[Tooltip("Multiplies the base Data.ChewTime of the Cookie. It affects how much time the dog will spend sat at the desk. Expressed in seconds")]
		public int TimeMultiplier = 1;

		public Cookie Data;
	}
}
