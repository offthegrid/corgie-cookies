﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Criterion
{
	public class DeskInstance : MonoBehaviour
	{
		[Tooltip("When a cookie is placed on the desk it will be referenced here. Null it means no cookie is placed")]
		public CookieInstance CookieSlot;

		[Tooltip("When a dog is sit at the desk it will be referenced here. Null it means the desk is available")]
		public DogInstance DogSlot;

		public Desk Data;

		private float m_timeBeforeNextScoreReport = 1.0f;
		private float m_timeSpentWorking = 0.0f;

		/// <summary>
		/// Return true if there is a dog assigned to the desk
		/// </summary>
		/// <returns>bool</returns>
		public bool HasDog()
		{
			return DogSlot != null;
		}

		/// <summary>
		/// Return true if there is cookie placed on the desk
		/// </summary>
		/// <returns>bool</returns>
		public bool HasCookie()
		{
			return CookieSlot != null;
		}

		/// <summary>
		/// Return the dog working at the desk
		/// </summary>
		/// <returns>DogInstance</returns>
		public DogInstance GetDog()
		{
			return DogSlot;
		}

		/// <summary>
		/// Return the cookie placed on the desk
		/// </summary>
		/// <returns>CookieInstance</returns>
		public CookieInstance GetCookie()
		{
			return CookieSlot;
		}

		public int GetProductivity()
		{
			if (!HasDog())
			{
				return 0;
			}

			return (int)(Data.BaseScore * DogSlot.GetProductivity()); //TODO: check that the formula makes sense
		}

		public void OnDeskSelected()
		{
			PlayerActionManager.Instance.OnDeskSelected(this);
		}

		public void OnCookiePlaced(CookieInstance cookie)
		{
			CookieSlot = cookie;

			foreach (var listener in m_listeners)
			{
				listener.OnCookiePlacedOnDesk(this, cookie);
			}
		}

		IEnumerator ScheduleDogAssignment(DogInstance dog, float afterTime)
		{
			yield return new WaitForSeconds(afterTime);
			OnDogAssigned(dog);
		}

		public void OnDogAssigned(DogInstance dog)
		{
			DogSlot = dog;

			foreach (var listener in m_listeners)
			{
				listener.OnDogAssignedToDesk(this, dog);
			}

			// TODO: dog start working and the cookie start depleting
			// for now just simulate that the dog will arrive at its place after a while
		}

		public void ChewOnCookie(float deltaTime)
		{
			m_timeSpentWorking += deltaTime;
			m_timeBeforeNextScoreReport -= deltaTime;

			var totalCookieDuration = CookieSlot.TimeMultiplier * CookieSlot.Data.ChewTime;

			if (m_timeBeforeNextScoreReport <= 0)
			{
				ScoreManager.Instance.ReportScore(1); //report a base value for now
				m_timeBeforeNextScoreReport = 1.0f; //next report will be in 1 second

				foreach (var listener in m_listeners)
				{
					listener.OnCookieChewed(Mathf.Clamp(1.0f - Mathf.Min(m_timeSpentWorking/totalCookieDuration, 1.0f), 0.0f, 1.0f));
				}
			}

			// if dog has finished the cookie we release him
			if (m_timeSpentWorking >= totalCookieDuration)
			{
				m_timeSpentWorking = 0.0f;
				m_timeBeforeNextScoreReport = 1.0f;
				RemoveCookieFromDesk();
				RemoveDogFromDesk();
			}
		}

		public void RemoveDogFromDesk()
		{
			foreach (var listener in m_listeners)
			{
				listener.OnDogLeftDesk(this, DogSlot);
			}
			
			DogSlot = null;
		}

		public void RemoveCookieFromDesk()
		{
			CookieSlot = null;
		}

		#region Listener
		private List<DeskInstanceListener> m_listeners = new List<DeskInstanceListener>();

		public void AddListener(DeskInstanceListener listener)
		{
			if (m_listeners.Contains(listener))
			{
				return;
			}

			m_listeners.Add(listener);
		}

		public void RemoveListener(DeskInstanceListener listener)
		{
			if (!m_listeners.Contains(listener))
			{
				return;
			}

			m_listeners.Remove(listener);
		}

		public interface DeskInstanceListener
		{
			void OnDogAssignedToDesk(DeskInstance desk, DogInstance dog);
			void OnDogLeftDesk(DeskInstance desk, DogInstance dog);
			void OnCookiePlacedOnDesk(DeskInstance desk, CookieInstance cookie);
			void OnCookieChewed(float cookieRemaining);
		}
		#endregion //Listener

	}
}
