﻿using System.Collections.Generic;
using UnityEngine;

namespace Criterion
{
    [CreateAssetMenu(fileName = "SpriteCollection", menuName = "Data/SpriteCollection")]
    public class SpriteCollection : ScriptableObject
    {
        [Tooltip("All potential sprites for the dogs")]
        public DogSprites[] Sprites;


        [Tooltip("All potential jobs for the dogs")]
        public JobSprites[] JobBubbles;
    }
}
