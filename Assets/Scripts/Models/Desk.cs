﻿using UnityEngine;

namespace Criterion
{
	[CreateAssetMenu(fileName = "Desk", menuName = "Data/Desk")]
	public class Desk : ScriptableObject
	{
		[Tooltip("Base score that the desk generate at every tick when it's occupied by a dog working")]
		public int BaseScore = 10;

		[Tooltip("Default Sprite that represent the desk")]
		public Sprite Sprite;
	}
}
