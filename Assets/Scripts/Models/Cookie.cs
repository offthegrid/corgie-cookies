﻿using UnityEngine;

namespace Criterion
{
	[CreateAssetMenu(fileName = "Cookie", menuName = "Data/Cookie")]
	public class Cookie : ScriptableObject
	{
		[Tooltip("How long will it take to the dog to eat the cookie (expressed in seconds)")]
		public int ChewTime;

		[Tooltip("What role of dogs it attracts (code, art, design)")]
		public Role Role;

		[Tooltip("Default Sprite that represent the cookie")]
		public Sprite Sprite;

		[Tooltip("Default Sprite that represent when you have many cookies together")]
		public Sprite ManySprite;
	}
}
