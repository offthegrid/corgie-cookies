﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Criterion;

public class DeskRegistry : MonoBehaviour
{
    public List<GameObject> seatLocators = new List<GameObject>();

    public DeskInstance Instance = null;

    // Start is called before the first frame update
    void Start()
    {
    }

    private void OnDestroy()
    {
        FloorManager floorMgr = FloorManager.Instance;
        if (floorMgr)
            floorMgr.RemoveDesk(this);
    }

    // Update is called once per frame
    void Update()
    {
        FloorManager floorMgr = FloorManager.Instance;
        if (floorMgr)
        {
            // Are we part of a group? If we are, skip registering (the group will be reflected in the 2D scene, not each desk)
            if (gameObject.transform.GetComponentInParent<DeskGroupRegistry>() == null)
                floorMgr.AddDesk(this);

            this.enabled = false;
        }
    }

    public void GetSeatPositions(IList<FloorManager.SeatInfo> deskSeatPositions)
    {
        foreach (GameObject seat in seatLocators)
        {
            FloorManager.SeatInfo info = new FloorManager.SeatInfo();
            info.sourceDesk = this;
            info.position = seat.gameObject.transform.position;
            deskSeatPositions.Add(info);
        }
    }
}
