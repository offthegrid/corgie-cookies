﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Criterion;

public class DogWalker : MonoBehaviour
{
    public float walkSpeed = 1.0f;
    public float averageWaitTime = 1.0f;
    public float waitTimeVariation = 0.25f;
    public float arrivalThresholdDistance = 0.1f;
    public AudioSource audio;
    public AudioClip BorkOne;
    public AudioClip BorkTwo;

    class SeatMapping
    {
        public DeskRegistry sourceDesk;
        public Vector3 originalSeatPos;
        public Vector3 navPos;
    }
    List<SeatMapping> seats = new List<SeatMapping>();

    enum walkState
    {
        Init,
        Waiting,
        Wandering,
        HeadingToTarget,
        Sitting
    }
    walkState state = walkState.Init;

    float timeRemainingInState = 0.0f;
    Vector2 currentTarget = new Vector2();
    SeatMapping currentTargetSeat = null;

    protected NavMeshAgent navMeshAgent { get; private set; }

    public DogInstance Instance = null;

    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent = null;

        timeRemainingInState = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case walkState.Init: UpdateInit(); break;
            case walkState.Waiting: UpdateWaiting(); break;
            case walkState.Wandering: UpdateWandering(); break;
            case walkState.HeadingToTarget: UpdateTarget(); break;
            case walkState.Sitting: UpdateSitting(); break;
        }
    }

    private void OnDestroy()
    {
        FloorManager floorMgr = FloorManager.Instance;
        if (floorMgr != null)
        {
            floorMgr.RemoveDog(this);
        }      
    }

    void UpdateInit()
    {
        // Bit of filth - give the floor model for the level time to settle down before we query it (to avoid start-up order issues)
        timeRemainingInState -= Time.deltaTime;
        if (timeRemainingInState > 0.0f)
            return;

        FloorManager floorMgr = FloorManager.Instance;
        if (floorMgr == null)
        {
            return;
        }
        floorMgr.AddDog(this);

        // Try to attach agent to navMesh
        NavMeshHit initialHit;
        bool startPointFound = false;
        float checkRadius = 0.25f;
        do
        {
            startPointFound = NavMesh.SamplePosition(this.gameObject.transform.position, out initialHit, 1.0f, NavMesh.AllAreas);
            checkRadius += 0.25f;
        }
        while (!startPointFound);

        // Add, position, and configure agent
        this.transform.position = initialHit.position;
        navMeshAgent = this.gameObject.AddComponent<NavMeshAgent>();
        navMeshAgent.obstacleAvoidanceType = ObstacleAvoidanceType.NoObstacleAvoidance;

        // Find the best nav mesh point for each desk seat
        List<FloorManager.SeatInfo> deskSeatPositions = new List<FloorManager.SeatInfo>();
        FloorManager.Instance.GetDeskSeats(deskSeatPositions);
        foreach(FloorManager.SeatInfo seatInfo in deskSeatPositions)
        {
            SeatMapping newSeat = new SeatMapping();
            newSeat.sourceDesk = seatInfo.sourceDesk;
            newSeat.originalSeatPos = seatInfo.position;
            bool success = NavMesh.SamplePosition(seatInfo.position, out initialHit, 1.0f, NavMesh.AllAreas);
            if (success)
            {
                newSeat.navPos = initialHit.position;
                seats.Add(newSeat);
            }
            else 
            {
                Debug.LogError("Can't map seat " + (seats.Count) + " to nav mesh");
            }
        }

        state = walkState.Waiting;
    }

    void UpdateWaiting()
    {
        timeRemainingInState -= Time.deltaTime;

        if (timeRemainingInState <= 0.0f)
        {
            if (FindWanderTarget())
            {
                if (Instance != null)
                    Instance.PathChanged();

                state = walkState.Wandering;
            }
        }
    }

    void UpdateWandering()
    {
        // Are we currently following a valid path ?
        if (navMeshAgent.pathStatus == NavMeshPathStatus.PathComplete && (navMeshAgent.remainingDistance < arrivalThresholdDistance))
        {
            timeRemainingInState = Random.Range(averageWaitTime - waitTimeVariation, averageWaitTime + waitTimeVariation);
            navMeshAgent.enabled = false;
            state = walkState.Waiting;
            switch (Random.Range(0, 2))
            {
                case 0:
                    audio.PlayOneShot(BorkOne);
                    break;
                case 1:
                    audio.PlayOneShot(BorkTwo);
                    break;
                case 2:
                    break;
                default:
                    break;
            }
        }
    }

    void UpdateTarget()
    {
        // Have we arrived? 
        if (navMeshAgent.pathStatus == NavMeshPathStatus.PathComplete && (navMeshAgent.remainingDistance < arrivalThresholdDistance))
        {
            if (Instance != null)
            {
                Debug.Log("Dog " + DebugName + " arrived at seat and wants to sit!");
                Instance.SetSitting(true);

                // Tell the desk which dog is sat at it
                if (currentTargetSeat.sourceDesk.Instance)
                    currentTargetSeat.sourceDesk.Instance.OnDogAssigned(Instance);

                navMeshAgent.obstacleAvoidanceType = ObstacleAvoidanceType.NoObstacleAvoidance;
                state = walkState.Sitting;
            }
            else
            {
                navMeshAgent.enabled = false;
                state = walkState.Waiting;
            }            
        }
    }

    void UpdateSitting()
    {
        // Does the dog want to leave it's seat?
        if (Instance)
        {
            if (Instance.IsDistracted())
            {
                // Tell the desk the dog has left
                if (currentTargetSeat.sourceDesk.Instance)
                {
                    currentTargetSeat.sourceDesk.Instance.OnDogAssigned(null);
                }

                LeaveDesk();
            }            
        }
    }

    public void LeaveDesk()
    {
        currentTargetSeat = null;
        navMeshAgent.enabled = false;
        navMeshAgent.obstacleAvoidanceType = ObstacleAvoidanceType.NoObstacleAvoidance;

        state = walkState.Waiting;
    }

    public bool IsReadyForTarget
    {
        get { return state == walkState.Waiting || state == walkState.Wandering; }
    }

    public bool SetTargetDesk(DeskInstance targetInstance)
    {
        // Map instance to known seat
        SeatMapping targetMapping = seats.Find(a => (a.sourceDesk.Instance != null && a.sourceDesk.Instance == targetInstance));
        if (targetMapping == null)
        {
            Debug.LogError("Dog " + DebugName + " asked to nav to unknown desk " + targetInstance.gameObject.name);
            return false;
        }

        // Navigate
        if (NavToTarget(targetMapping.navPos))
        {
            Debug.Log("Dog " + DebugName + " wants to sat in desk " + targetInstance.gameObject.name + " at " + targetMapping.navPos);
            currentTargetSeat = targetMapping;

            state = walkState.HeadingToTarget;
            return true;
        }
        else
        {
            Debug.LogError("Can't navigate to target at " + targetMapping.navPos);
            return false;
        }
    }

    bool TryFindTarget()
    {
        bool foundTarget = false;
        SeatMapping newTarget = null;

        // Look for interesting cookies
        // TODO - seat finding. For now, just a low chance of wanting to sit
        float want = Random.Range(0.0f, 100.0f);
        if (want < 1.0f)
        {
            // Find position of desk with cookies
            int seatIndex = Random.Range(0, seats.Count - 1);
            newTarget = seats[seatIndex];
            foundTarget = true;

            Debug.Log("Dog " + DebugName + " wants to sat in desk " + seatIndex + " at " + newTarget + "(random want was " + want + ")");
        }

        // GO! GOOD BOY!
        if (foundTarget)
        {
            if (NavToTarget(newTarget.navPos))
            {
                currentTargetSeat = newTarget;
                //Debug.Log("Dog " + DebugName + " is heading to " + currentTarget);
                return true;
            }
            else
            {
                Debug.LogError("Can't navigate to target at " + newTarget);
            }
        }

        return false;
    }

    bool FindWanderTarget()
    {
        // Pick a "random" point to walk to
        FloorManager floorMgr = FloorManager.Instance;
        Vector2 extents = floorMgr.GetOfficeExtents();
        Vector3 target = new Vector3(Random.Range(-extents.x, extents.x), 1.0f, Random.Range(-extents.y, extents.y));
        if (NavToTarget(target))
        {
            //Debug.Log("Dog " + DebugName + " is wandering to " + currentTarget);
            return true;
        }

        return false;
    }

    bool NavToTarget(Vector3 target)
    {
        //NavMeshHit navMeshHit;
        //if (NavMesh.Raycast(gameObject.transform.position, target, out navMeshHit, NavMesh.AllAreas) == false) // false = no obstruction between source & dest
        {
            navMeshAgent.enabled = true;
            navMeshAgent.SetDestination(target); // navMeshHit.position);
            ReconfigureAgent();
            currentTarget = target; // navMeshHit.position;

            return true;
        }

        //return false;
    }

    string DebugName
    {
        get { return this.gameObject.name; }
    }

    void ReconfigureAgent()
    {
        navMeshAgent.acceleration = Random.Range(4.0f, 10.0f);
        navMeshAgent.speed = Random.Range(2.0f, 5.0f);
        navMeshAgent.angularSpeed = 360.0f;
    }

    Role role = Role.Art;
    int skin = -1;
    public void SetProperties(Role oldRole, int oldSkin)
    {
        role = oldRole;
        skin = oldSkin;
    }

    public Role Role { get { return role;  } }

    public void ConfigureInstance()
    {
        Instance.SetDogRole(role);
        Instance.SetDogSkin(skin);
    }


    public enum Orientation
    {
        Left,
        Right
    }
    public Orientation DogDirection
    {
        get
        {
            float xProjection = Vector3.Dot(this.gameObject.transform.forward, new Vector3(1.0f, 0.0f, 0.0f));
            return (xProjection > 0.0f) ? Orientation.Right : Orientation.Left; 
        }
    }
}
