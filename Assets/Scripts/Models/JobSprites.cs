﻿using System.Collections.Generic;
using UnityEngine;

namespace Criterion
{
    [CreateAssetMenu(fileName = "JobSprites", menuName = "Data/JobSprites")]
    public class JobSprites : ScriptableObject
    {
        [Tooltip("What role this job has")]
        public Role Role;

        [Tooltip("A visual representation of the job")]
        public Sprite RoleImage;
    }
}