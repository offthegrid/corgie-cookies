﻿using UnityEngine;

namespace Criterion
{
    public enum GameModeType
    {
        Setup,
        StartMenu,
        Game,
        Score,
        Credits
    }

    [CreateAssetMenu(fileName = "GameMode", menuName = "Data/GameMode")]
    public class GameMode : ScriptableObject
    {
        [Tooltip("Type of game mode this description belongs to")]
        public GameModeType Type;

        [Tooltip("List of scenes to load when the game mode is active")]
        public string[] SceneList;
    }
}
