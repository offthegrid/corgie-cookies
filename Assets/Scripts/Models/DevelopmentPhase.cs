﻿using UnityEngine;
using UnityEngine.UI;

namespace Criterion
{
    [CreateAssetMenu(fileName = "DevelopmentPhase", menuName = "Data/DevelopmentPhase")]
    public class DevelopmentPhase : ScriptableObject
    {
        [Tooltip("Name of the Development phase")]
        public string Name;

        [Tooltip("Scene to load when the phase is over")]
        public string SceneName;

        [Tooltip("Duration of the phase in seconds")]
        public float Duration;

        [Tooltip("Sprite to be used in the development marker of the progress bar")]
        public Sprite PhaseSprite;

        [Tooltip("How many new artist dogs to gen this phase")]
        public int NewArtists = 0;

        [Tooltip("How many new SE dogs to gen this phase")]
        public int NewEngineers = 0;

        [Tooltip("How many new design dogs to gen this phase")]
        public int NewDesigners = 0;
    }
}
