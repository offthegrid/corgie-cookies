﻿using System;
using System.Collections;
using System.Collections.Generic;
using Criterion;
using UnityEngine;
using static Criterion.PlayerActionManager;
using static Criterion.DeskInstance;

public class DogManager : MonoBehaviour, DeskEventsListener, DeskInstanceListener, FloorManager.Listener
{
	static DogManager instance = null;
	public static DogManager Instance { get { return instance; } }

	class DeskToDog
	{
		public DeskInstance desk = null;
		public DogWalker dog = null;
	}
	List<DeskToDog> desksToDogs = new List<DeskToDog>();

	class LooseCookie
	{
		public DeskInstance desk;
		public CookieInstance cookie;
	}
	List<LooseCookie> looseCookies = new List<LooseCookie>();

	private void OnDestroy()
	{
		PlayerActionManager playerActionMgr = PlayerActionManager.Instance;
		if (playerActionMgr)
		{
			playerActionMgr.RemoveListener(this);
		}

		FloorManager floorMgr = FloorManager.Instance;
		if (floorMgr)
		{
			floorMgr.RemoveListener(this);
		}
	}

	// Update is called once per frame
	void Update()
	{
		PlayerActionManager playerActionMgr = PlayerActionManager.Instance;
		if (playerActionMgr)
		{
			playerActionMgr.AddListener(this);
		}

		FloorManager floorMgr = FloorManager.Instance;
		if (floorMgr)
		{
			floorMgr.AddListener(this);
		}
	}

	public void OnCookiePlacedOnDesk(DeskInstance desk, CookieInstance cookie)
	{
		FloorManager floorMgr = FloorManager.Instance;
		if (floorMgr == null)
		{
			return;
		}

		// Find a dog that's interested in this cookie and assign it
		List<DogWalker> currentDogs = new List<DogWalker>();
		floorMgr.GetDogs(currentDogs);
		foreach (DogWalker dog in currentDogs)
		{
			//if (dog likes this cookie)
			if ((dog.IsReadyForTarget) && (dog.Role == cookie.Data.Role))
			{
				if (dog.SetTargetDesk(desk))
				{
					DeskToDog deskToDog = new DeskToDog();
					deskToDog.desk = desk;
					deskToDog.dog = dog;
					desksToDogs.Add(deskToDog);

					// We want to know when this desk's work is complete
					desk.AddListener(this);
					return;
				}
			}
		}

		// Couldn't assign, cache for later    
		//         LooseCookie newCookie = new LooseCookie();
		//         newCookie.desk = desk;
		//         newCookie.cookie = cookie;
		//         looseCookies.Add(newCookie);
	}

	public void OnDogAssignedToDesk(DeskInstance desk, DogInstance dog)
	{ }

	public void OnDogLeftDesk(DeskInstance desk, DogInstance dog)
	{
		DeskToDog mapping = desksToDogs.Find(a => a.desk = desk);

		if (mapping != null)
		{
			DogWalker freeDog = mapping.dog;
			mapping.dog.LeaveDesk();
			desksToDogs.Remove(mapping);

			// Is there another cookie for this doggie to eat?
			LooseCookie cached = looseCookies.Find(a => a.cookie.Data.Role == freeDog.Role);
			if (cached != null)
			{
				if (freeDog.IsReadyForTarget)
				{
					if (freeDog.SetTargetDesk(cached.desk))
					{
						DeskToDog deskToDog = new DeskToDog();
						deskToDog.desk = desk;
						deskToDog.dog = freeDog;
						desksToDogs.Add(deskToDog);

						// We want to know when this desk's work is complete
						desk.AddListener(this);
					}
				}
				looseCookies.Remove(cached);
			}
		}
	}

	//void MatchDogToDesk()

	public void OnCookieChewed(float cookieRemaining)
	{ }

	public void OnDogsChanged()
	{
		FloorManager floorMgr = FloorManager.Instance;
		if (floorMgr == null)
		{
			return;
		}

		// Find a dog that's interested in this cookie and assign it
		List<DogWalker> current = new List<DogWalker>();
		floorMgr.GetDogs(current);

		List<DeskToDog> toDelete = new List<DeskToDog>();
		foreach (DeskToDog mapping in desksToDogs)
		{
			if (current.Contains(mapping.dog) == false)
				toDelete.Add(mapping);
		}

		desksToDogs.RemoveAll(a => toDelete.Contains(a));
	}

	public void OnDesksChanged()
	{
		FloorManager floorMgr = FloorManager.Instance;
		if (floorMgr == null)
		{
			return;
		}

		// Find a dog that's interested in this cookie and assign it
		List<DeskRegistry> current = new List<DeskRegistry>();
		floorMgr.GetDesks(current);

		List<DeskToDog> toDelete = new List<DeskToDog>();
		foreach (DeskToDog mapping in desksToDogs)
		{
			if (current.Exists(a => (a.Instance != null) && (a.Instance == mapping.desk)) == false)
				toDelete.Add(mapping);
		}

		desksToDogs.RemoveAll(a => toDelete.Contains(a));
	}

	public void OnDeskGroupsChanged()
	{
		FloorManager floorMgr = FloorManager.Instance;
		if (floorMgr == null)
		{
			return;
		}

		// Find a dog that's interested in this cookie and assign it
		List<DeskGroupRegistry> currentGroups = new List<DeskGroupRegistry>();
		floorMgr.GetDeskGroups(currentGroups);

		List<DeskRegistry> current = new List<DeskRegistry>();
		foreach (DeskGroupRegistry group in currentGroups)
		{
			group.GetDeskRegistries(current);
		}

		List<DeskToDog> toDelete = new List<DeskToDog>();
		foreach (DeskToDog mapping in desksToDogs)
		{
			if (current.Exists(a => (a.Instance != null) && (a.Instance == mapping.desk)) == false)
				toDelete.Add(mapping);
		}

		desksToDogs.RemoveAll(a => toDelete.Contains(a));
	}
}
