﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;
using System.Linq;

namespace Criterion
{
	[CreateAssetMenu(fileName = "ScoreManager", menuName = "Data/Score Manager")]
	public class ScoreManager : ScriptableObject
	{
		private static ScoreManager _instance;
		public static ScoreManager Instance
		{
			get
			{
				if (!_instance)
				{
					_instance = Resources.FindObjectsOfTypeAll<ScoreManager>().FirstOrDefault();
				}

#if UNITY_EDITOR
				if (!_instance)
				{
					InitializeFromDefault(UnityEditor.AssetDatabase.LoadAssetAtPath<ScoreManager>("Assets/Data/Default Score Manager.asset"));
				}
#endif
				Assert.IsTrue(_instance != null, "Unable to retrieve a valid Player Action Manager");
				return _instance;
			}
			set
			{
				_instance = value;
			}
		}

		public static void InitializeFromDefault(ScoreManager manager)
		{
			if (_instance) DestroyImmediate(_instance);
			_instance = Instantiate(manager);
			_instance.hideFlags = HideFlags.HideAndDontSave;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////
		public int TargetScore = 100;
		public int CurrentScore = 0;

		public int GetCurrentScore()
		{
			return CurrentScore;
		}

		public void ReportScore(int score)
		{
			CurrentScore += score;
			CurrentScore = (int)Mathf.Clamp(CurrentScore, 0.0f, TargetScore);

			foreach (var listener in m_scoreEventListeners)
			{
				listener.OnScoreChanged(score, CurrentScore);
			}

			if (CurrentScore < TargetScore)
			{
				return;
			}

			foreach (var listener in m_scoreEventListeners)
			{
				listener.OnScoreTargetHit(score, CurrentScore, TargetScore);
			}
		}

		#region Listener
		private List<ScoreEventListener> m_scoreEventListeners = new List<ScoreEventListener>();

		public void AddListener(ScoreEventListener listener)
		{
			if (m_scoreEventListeners.Contains(listener))
			{
				return;
			}

			m_scoreEventListeners.Add(listener);
		}

		public void RemoveListener(ScoreEventListener listener)
		{
			if (!m_scoreEventListeners.Contains(listener))
			{
				return;
			}

			m_scoreEventListeners.Remove(listener);
		}

		public interface ScoreEventListener
		{
			void OnScoreChanged(int lastContribute, int currentScore);
			void OnScoreTargetHit(int lastContribute, int currentScore, int targetScore);
		}
		#endregion //Listener
	}
}
