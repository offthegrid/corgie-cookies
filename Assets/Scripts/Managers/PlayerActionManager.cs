﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;
using System.Linq;

namespace Criterion
{
	[CreateAssetMenu(fileName = "PlayerActionManager", menuName = "Data/Player Action Manager")]
	public class PlayerActionManager : ScriptableObject
	{
		private static PlayerActionManager _instance;
		public static PlayerActionManager Instance
		{
			get
			{
				if (!_instance)
				{
					_instance = Resources.FindObjectsOfTypeAll<PlayerActionManager>().FirstOrDefault();
				}

#if UNITY_EDITOR
				if (!_instance)
				{
					InitializeFromDefault(UnityEditor.AssetDatabase.LoadAssetAtPath<PlayerActionManager>("Assets/Default Player Action Manager.asset"));
				}
#endif
				Assert.IsTrue(_instance != null, "Unable to retrieve a valid Player Action Manager");
				return _instance;
			}
			set
			{
				_instance = value;
			}
		}

		public static void InitializeFromDefault(PlayerActionManager manager)
		{
			if (_instance) DestroyImmediate(_instance);
			_instance = Instantiate(manager);
			_instance.hideFlags = HideFlags.HideAndDontSave;
		}

		private CookieInstance m_activeCookie;

		public void OnCookieGrabbed(CookieJarInstance jar, CookieInstance cookie)
		{
			m_activeCookie = cookie;

			foreach (var listener in m_cookieJarEventsListeners)
			{
				listener.OnCookieGrabbed(jar, m_activeCookie);
			}
		}

		public void OnCookieBought(CookieJarInstance jar, CookieInstance cookie, int amount)
		{
			foreach (var listener in m_cookieJarEventsListeners)
			{
				listener.OnCookieBought(jar, m_activeCookie, amount);
			}

			m_activeCookie = null;
		}

		public void OnDeskSelected(DeskInstance desk)
		{
			if (m_activeCookie == null || m_activeCookie.Data.Role == Role.Leaf ||  desk.HasCookie() || desk.HasDog())
			{
				return;
			}

			if(m_activeCookie != null)
			{
				desk.OnCookiePlaced(m_activeCookie);

				foreach (var listener in m_deskEventsListeners)
				{
					listener.OnCookiePlacedOnDesk(desk, m_activeCookie);
				}

				m_activeCookie = null;
			}
		}

		public void OnLeafGrabbedFromPlant(CookieInstance leaf)
		{
			m_activeCookie = leaf;

			foreach (var listener in m_cookieJarEventsListeners)
			{
				listener.OnCookieGrabbed(null, m_activeCookie);
			}
		}

		public CookieInstance GetActiveCookie()
		{
			return m_activeCookie;
		}

		//////////////////////////////////////////////////////////////////////////////////

		#region Listener
		private List<DeskEventsListener> m_deskEventsListeners = new List<DeskEventsListener>();

		public void AddListener(DeskEventsListener listener)
		{
			if (m_deskEventsListeners.Contains(listener))
			{
				return;
			}

			m_deskEventsListeners.Add(listener);
		}

		public void RemoveListener(DeskEventsListener listener)
		{
			if (!m_deskEventsListeners.Contains(listener))
			{
				return;
			}

			m_deskEventsListeners.Remove(listener);
		}

		public interface DeskEventsListener
		{
			void OnCookiePlacedOnDesk(DeskInstance desk, CookieInstance cookie);
		}
		
		//////////////////////////////////////////////////////////////////////////////////
		
		private List<CookieJarEventsListener> m_cookieJarEventsListeners = new List<CookieJarEventsListener>();

		public void AddCookieJarListener(CookieJarEventsListener listener)
		{
			if (m_cookieJarEventsListeners.Contains(listener))
			{
				return;
			}

			m_cookieJarEventsListeners.Add(listener);
		}

		public void RemoveCookieJarListener(CookieJarEventsListener listener)
		{
			if (!m_cookieJarEventsListeners.Contains(listener))
			{
				return;
			}

			m_cookieJarEventsListeners.Remove(listener);
		}

		public interface CookieJarEventsListener
		{
			void OnCookieGrabbed(CookieJarInstance jar, CookieInstance cookie);
			void OnCookieBought(CookieJarInstance jar, CookieInstance cookie, int amount);
		}
		#endregion //Listener
	}
}
