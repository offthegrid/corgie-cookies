﻿using System.Collections.Generic;
using UnityEngine;

namespace Criterion
{
	public abstract class RuntimeSet<T> : ScriptableObject
	{
		public interface RuntimeSetListener<T>
		{
			void OnItemAdded(T item, RuntimeSet<T> set);
			void OnItemRemoved(T item, RuntimeSet<T> set);
			void OnRuntimeSetCleared(RuntimeSet<T> set);
		}

		public List<T> Items = new List<T>();
		private readonly List<RuntimeSetListener<T>> m_listeners = new List<RuntimeSetListener<T>>();

		public void Add(T item)
		{
			if (!Items.Contains(item))
			{
				Items.Add(item);

				foreach (var listener in m_listeners)
				{
					listener.OnItemAdded(item, this);
				}
			}
		}

		public void Remove(T item)
		{
			if (Items.Contains(item))
			{
				Items.Remove(item);

				foreach (var listener in m_listeners)
				{
					listener.OnItemRemoved(item, this);
				}
			}
		}

		public void Clear()
		{
			Items.Clear();
			foreach (var listener in m_listeners)
			{
				listener.OnRuntimeSetCleared(this);
			}
		}

		public void AddListener(RuntimeSetListener<T> listener)
		{
			if (m_listeners.Contains(listener))
			{
				return;
			}

			m_listeners.Add(listener);
		}

		public void RemoveListener(RuntimeSetListener<T> listener)
		{
			if (!m_listeners.Contains(listener))
			{
				return;
			}

			m_listeners.Remove(listener);
		}

		public IEnumerator<T> GetEnumerator()
		{
			return Items.GetEnumerator();
		}


	}
}