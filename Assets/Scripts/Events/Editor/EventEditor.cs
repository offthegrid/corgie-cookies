﻿using UnityEditor;
using UnityEngine;

namespace Criterion
{
	[CustomEditor(typeof(GameEvent))]
	public class EventEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			GUI.enabled = UnityEngine.Application.isPlaying;

			GameEvent e = target as GameEvent;
			if (GUILayout.Button("Raise"))
				e.Raise();
		}
	}
}